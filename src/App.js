import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import TodosList from "./TodosList";
import CreateTodo from "./CreateTodo";
import EditTodo from "./EditTodo"
import Login from './Login';
import Register from './Register';
import Home from './Home';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="container">
          <nav className="navbar navbar-expand-lg navbar-light bg-light">
            
            <Link to="/" className="navbar-brand">App Frontend(MERN)</Link>
            <div className="collpase navbar-collapse">
              <ul className="navbar-nav mr-auto">
                
                <li className="navbar-item">
                    <Link to="/home" className="nav-link">Home</Link>
                  </li>  
                
                <li className="navbar-item">
                  <Link to="/todolist" className="nav-link">Todos</Link>
                </li>
                
                <li className="navbar-item">
                  <Link to="/create" className="nav-link">NewTodo</Link>
                </li>
                <li className="navbar-item">
                    <Link to="/login" className="nav-link">Login</Link>
                  </li>
                <li className="navbar-item">
                    <Link to="/register" className="nav-link">Register</Link>
                </li>  
                {/* <li className="navbar-item">
                    <Link to="/login1" className="nav-link">Login1</Link>
                  </li>
                  <li className="navbar-item">
                    <Link to="/register1" className="nav-link">Register1</Link>
                </li>    */}

              </ul>
            </div>
          </nav>
          <br/>
          <Route path="/todolist"  component={TodosList} />
          <Route path="/edit/:id" component={EditTodo} />
          <Route path="/create" component={CreateTodo} />
          <Route path="/login" exact component={Login} />
          <Route path="/register" component={Register} />
          <Route path="/home" component={Home} />
        </div>
      </Router>
    );
  }
}

export default App;