import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';


class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      login: false,
      errors: {}
    };
  }
  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };
  onSubmit = e => {
    e.preventDefault();
    const userData = {
      email: this.state.email,
      password: this.state.password
    };
    console.log(userData);
    axios.post('http://localhost:3000/users/login', userData)
    .then(res => {
        if (res.data.header === "SUCCESS") {
            this.props.history.push("/home");
        }
        
            console.log(res.data)
        
    });


  };
  render() {
    const { errors } = this.state;
    return (
      <div style={{ marginTop: 3 }}>
        <div className="col s8 offset-s2">
          <Link to="/register" className="btn-flat waves-effect">
            <label >Don't have a count</label>
          </Link>
          <form noValidate onSubmit={this.onSubmit}>
            <div className="form-group">
              <label>Email: </label><br />
              <input
                onChange={this.onChange}
                value={this.state.email}
                error={errors.username}
                id="email"
                type="email"
              /><br />
            </div>

            <div className="form-group">
              <label>Password: </label><br />
              <input
                // style={{
                //   width: "150px",
                //   marginLeft: "10rem"
                // }}
                onChange={this.onChange}
                value={this.state.password}
                error={errors.password}
                id="password"
                type="password"
              />
            </div>
            <div className="form-group">
              <label>Email: </label><br />
              <Calendar format='DD/MM/YYYY' date='4-12-2014' /><br />
            </div>

            <div className="form-group">
              <input type="submit" value="Create Todo" className="btn btn-primary" />
            </div>

          </form>
        </div>
      </div>

    );
  }
}
export default Login;