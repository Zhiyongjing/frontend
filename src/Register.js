import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
import * as EmailValidator from 'email-validator';

class Register extends Component {
    constructor() {
        super();
        this.state = {
            name: "",
            username: "",
            email: "",
            password: "",
            password2: "",
            role: "",
            errors: {}
        };
    }
    onChange = e => {
        this.setState({ [e.target.id]: e.target.value });
    };
    onSubmit = e => {
        e.preventDefault();
        const newUser = {
            name: this.state.name,
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
            password2: this.state.password2,
            role: this.state.role
        };
        
        if (newUser.password !== newUser.password2) {
            alert("Passwords don't match");

        }
        else if(newUser.name === "" || newUser.username ==="" || newUser.email === ""){
            alert("Name or username can not be empty!");
        }
        else if(newUser.roll ===""){
            alert("Please select the roll!");
        }
        else if (!(EmailValidator.validate(newUser.email))){
            alert("Please check email!");
        }
        else{
        axios.post('http://localhost:3000/users/register', newUser)
            .then(res => {
                if (res.data.header === "SUCCESS") {
                    this.props.history.push("/login");
                }
               
                    console.log(res.data)
                
            });




        console.log(newUser);}
    };
    render() {
        const { errors } = this.state;
        return (
            <div style={{ margin: "center" }}>
                <div className="col s8 offset-s2">
                    <Link to="/login" className="btn-flat waves-effect">
                        <label >Already have a count</label>
                    </Link>

                    <form noValidate onSubmit={this.onSubmit}>

                        <div className="form-group" >
                            <label>Name: </label><br />
                            <input onChange={this.onChange}
                                style={{ marginleft: 10 }}
                                value={this.state.name}
                                error={errors.name}
                                id="name"
                                type="text"
                            />
                        </div>

                        <div className="form-group">
                            <label>UserName: </label><br />
                            <input
                                onChange={this.onChange}
                                value={this.state.username}
                                error={errors.name}
                                id="username"
                                type="text"
                            />
                        </div>

                        <div className="form-group">
                            <label>Email: </label><br />
                            <input
                                onChange={this.onChange}
                                value={this.state.email}
                                error={errors.username}
                                id="email"
                                type="email"
                            />
                        </div>

                        <div className="form-group">
                            <label>Password: </label><br />
                            <input
                                onChange={this.onChange}
                                value={this.state.password}
                                error={errors.password}
                                id="password"
                                type="password"
                            />
                        </div>

                        <div className="form-group">
                            <label>Confirm password: </label><br />
                            <input
                                onChange={this.onChange}
                                value={this.state.password2}
                                error={errors.password}
                                id="password2"
                                type="password2"
                            />
                        </div>

                        <div className="form-check form-check-inline">
                            <input type="radio"
                                className="form-check-input"
                                name="react-tips"
                                id="role"
                                value="Business"
                                error={errors.role}
                                onChange={this.onChange}
                            />
                            <label className="form-check-label">Business</label>
                        </div>
                        <div className="form-check form-check-inline">
                            <input type="radio"
                                className="form-check-input"
                                name="react-tips"
                                id="role"
                                value="Personal"
                                error={errors.role}
                                onChange={this.onChange}
                            />
                            <label className="form-check-label">Personal</label>
                        </div>



                        <div className="form-group">
                            <input type="submit" value="Create Todo" className="btn btn-primary" />
                        </div>
                    </form>
                </div>

            </div>
        );
    }
}
export default Register;